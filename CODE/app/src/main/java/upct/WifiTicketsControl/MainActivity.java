package upct.WifiTicketsControl;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;   //**Automatically imported**
import android.os.Bundle;   //**Automatically imported**
import android.view.View;   //Used for the 'Views' in activity_main.xml
import android.widget.TextView;   //Used to display text boxes
import android.widget.Toast;   //Used to display toast notifications

// Java libraries for SSH2
import com.jcraft.jsch.ChannelExec; //Manages the channel to open a session
import com.jcraft.jsch.JSch;  //Pure Java implementation of SSH2
import com.jcraft.jsch.Session; //SSH session authentication

// Other libraries
import java.io.ByteArrayOutputStream;
import java.util.Properties;


public class MainActivity extends AppCompatActivity {

    /*======================
        Graphical elements
     =======================*/

    TextView pin;


    /*======================
        Program variables
     =======================*/

    final String USER = "root"; // Username
    final String PASS = "root"; // Password
    final String HOSTN = "192.168.10.108"; // Default router IP address is 192.168.135.1
    final int PT = 22; // Port (22 by default for SSH)

    String receivedOutput = ""; //Text output to be received


    /** SSH Method 'executeRemoteCommand' */
    public static String executeRemoteCommand(String username, String password, String hostname, int port) throws Exception {

        //SSH session parameters
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        //Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        //Initialise session
        session.connect();

        //Send commands through an SSH Channel
        ChannelExec channelssh = (ChannelExec) session.openChannel("exec");

        //The command will be transformed into an array of bytes
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);

        //Execute command
        channelssh.setCommand("./button_android_pressed");
        channelssh.connect();

        try {
            Thread.sleep(3000);
        }
        catch(Exception f){
            f.printStackTrace();
        }

        channelssh.disconnect();

        //Return the command output
        return baos.toString();
    }


    /**  METHOD called when the user presses the red button */
    public void buttonAndroidPressed(final View ImageButton) {

        //Deactivate the button until operation is done
        ImageButton.setClickable(false);
        ImageButton.setAlpha(.5f);

        new AsyncTask<Integer, Void, Void>() {
            @Override

            //PUTTING NETWORK COMMAND IN BACKGROUND TO AVOID UI FREEZING
            protected Void doInBackground(Integer... params) {
                //SSH CODE : EXECUTE COMMAND
                try {
                    receivedOutput = executeRemoteCommand(USER, PASS, HOSTN, PT); //Retrieve and save output of the router
                }

                //If there is an error inform the user
                catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            //AFTER THE COMMAND WAS SENT
            protected void onPostExecute(Void param){
                try {
                    pin.setText(receivedOutput + " min"); //Write output in text field

                    // Show 'toast' notification to inform the user button was pressed :
                    Toast.makeText(getApplicationContext(), "Button has been pressed !", Toast.LENGTH_SHORT).show();
                }

                //If there is an error, inform the user
                catch (Exception e) {
                    e.printStackTrace();
                }

                //Reactivate the button
                ImageButton.setClickable(true);
                ImageButton.setAlpha(1);
            }
        }.execute(1);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Automatic code

        pin = findViewById(R.id.pin_text);
    }
}