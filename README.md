# WifiTickets Android application

Welcome to this Git repository !

```
              |
            \ _ /
          -= (_) =-                 
            /   \                  ,~                             _\/_                             ,\//,  .\//\/.
              |                    |\                             //o\   _\/_             ,.,.,    //o\\  /o//o\\
    _  ___  __  _ __ __ _  _ __ _ /| \ __ __ _ _ _ _ __ _ __ _ __ _ | __ /o\\ _          /###/#\     |     |  |
  =-=-_=-=-_=-=_=-_=_=-_=-_-_=_=-/_|__\_=-=-==_==--==__==_==_=-=_,-'|"'""-|-,_ ^^^^^^^^^^|' '|:| ^^^^|^^^^^|^^|^^^^
   =- _=-=-_=- _=-= _--_ =-= -_=-=_-=__=- _=-= _--_ =-=,-"          |^^^^^|^^^^^^^^^^^^^^^^^^^^^^^^^^|`=.='|^^|^^^^
     =- =- =-= =- = -  -===- -= -= -===- =- =- -===-."   jgs  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

```


### Objectives

The objective of this project is to provide a mobile solution to the WifiTickets Platform.

![WifiTickets platform][wifiTickets_logo]

Before exploring the repository, please read this : http://www.wifitickets.net/


### Dependencies / Hardware / Software

In order to run the app we need :
- 1x WifiTickets Platform
- 1x Android device with **at minimum v4.4 KitKat (API 19)**
- Internet access

*__Note :__ Android device and WifiTickets router must be on the same network !*


## Installation and usage

<details>
<summary>If you own a WifiTicket platform and want to run the app on your phone/tablet</summary>

1. **Download this file :** [wifiTickets.APK](CODE/app/build/outputs/apk/debug/app-debug.apk)

2. **Install it on your Android phone/tablet**

</details>

<details>
<summary>If you are brave and want to dig into the code</summary>
    
1. Install **[GitKraken](https://www.gitkraken.com/)** and **[Android Studio](https://developer.android.com/studio/)**

2. In **GitKraken** clone the repository to your computer from the URL : https://gitlab.com/prsunflower/wifiTickets/

![GitKraken clone repository][instructions_1]


3. In **Android Studio** click on **_"Import project (Gradle, Eclipse, ADT, etc.)"_** and choose the *wifiTickets/CODE* folder on your computer.

![Android Studio "Import project"][instructions_2]

![Android Studio choose "CODE" folder][instructions_3]

</details>



## Todos

See here : [wifiTickets "isues"](https://gitlab.com/prsunflower/wifiTickets/boards)


## Authors

* Pr. Sunflower


## Links

WifiTickets Platform explanation : http://www.wifitickets.net/

Video demonstration : https://www.youtube.com/user/wifiTickets/videos?sort=da


## Contact

**[Dr. Francesc Burrull](mailto:francesc.burrull@upct.es)**





[wifiTickets_logo]: ILLUSTRATIONS/wifiTickets_logo.jpg
[instructions_1]: ILLUSTRATIONS/instructions_1_GitKraken.PNG
[instructions_2]: ILLUSTRATIONS/instructions_2_AS.PNG
[instructions_3]: ILLUSTRATIONS/instructions_3_AS.PNG
